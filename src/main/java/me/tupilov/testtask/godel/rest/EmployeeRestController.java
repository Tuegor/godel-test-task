package me.tupilov.testtask.godel.rest;

import me.tupilov.testtask.godel.entity.Employee;
import me.tupilov.testtask.godel.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeRestController {
    private EmployeeService employeeService;

    @Autowired
    public EmployeeRestController(EmployeeService theEmployeeService)
    {
        employeeService = theEmployeeService;
    }
    @GetMapping("/employees")
    public List<Employee> findAll()
    {
        return employeeService.findAll();
    }
    @GetMapping("/employees/{employeeId}")
    public Employee getEmployee(@PathVariable int employeeId)
    {
        Employee theEmployee = employeeService.findById(employeeId);
        if (theEmployee == null)
            throw new RuntimeException("Employee Id not found - " + employeeId);
        return theEmployee;
    }
    @PostMapping("/employees")
    public String addEmployee(@RequestBody Employee theEmployee)
    {
        theEmployee.setEmployeeId(0);
        employeeService.save(theEmployee);
        return "Inserted employee - " + theEmployee.getFirstName() + " " + theEmployee.getLastName();
    }
    @PutMapping("/employees")
    public String updateEmployee(@RequestBody Employee theEmployee)
    {
        Employee tempEmployee = employeeService.findById(theEmployee.getEmployeeId());
        if (tempEmployee == null)
            throw new RuntimeException("Employee Id not found - " + theEmployee.getEmployeeId());
        employeeService.save(theEmployee);
        return "Updated employee Id - " + theEmployee.getEmployeeId();
    }
    @DeleteMapping("/employees/{employeeId}")
    public String deleteEmployee(@PathVariable int employeeId)
    {
        Employee tempEmployee = employeeService.findById(employeeId);
        if (tempEmployee == null)
            throw new RuntimeException("Employee Id not found - " + employeeId);
        employeeService.deleteById(employeeId);
        return "Deleted employee Id - " + employeeId;
    }
}
