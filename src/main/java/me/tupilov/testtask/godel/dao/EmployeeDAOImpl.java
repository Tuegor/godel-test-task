package me.tupilov.testtask.godel.dao;

import me.tupilov.testtask.godel.entity.Employee;
import me.tupilov.testtask.godel.entity.Gender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.List;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO{
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<Employee> findAll() {
        List<Employee> employees = jdbcTemplate.query("SELECT * FROM employee", (rs, rowNum) -> new Employee(rs.getInt("employee_id"), rs.getString("first_name"), rs.getString("last_name"), rs.getInt("department_id"), rs.getString("job_title"), Gender.valueOf(rs.getString("gender")), rs.getDate("date_of_birth")) );
        return employees;
    }

    @Override
    public Employee findById(int theId) {
        List<Employee> employees = (List<Employee>) jdbcTemplate.query("SELECT * FROM employee WHERE employee_id = ?", new Integer[]{ theId }, new int[]{ Types.INTEGER }, (rs, rowNum) -> new Employee(rs.getInt("employee_id"), rs.getString("first_name"), rs.getString("last_name"), rs.getInt("department_id"), rs.getString("job_title"), Gender.valueOf(rs.getString("gender")), rs.getDate("date_of_birth")) );
        return employees.get(0);
}

    @Override
    public void save(Employee theEmployee) {
        if(theEmployee.getEmployeeId() == 0)
            jdbcTemplate.update(
                "INSERT INTO employee VALUES (default, ?, ?, ?, ?, CAST(? as gender_type), ?)",
                theEmployee.getFirstName(), theEmployee.getLastName(), theEmployee.getDepartmentId(), theEmployee.getJobTitle(), theEmployee.getGender().toString(), theEmployee.getDateOfBirth());
        else
            jdbcTemplate.update(
                    "UPDATE employee SET first_name = ?, last_name = ?, department_id = ?, job_title = ?, gender = CAST(? as gender_type), date_of_birth = ? where employee_id = ?",
                    theEmployee.getFirstName(), theEmployee.getLastName(), theEmployee.getDepartmentId(), theEmployee.getJobTitle(), theEmployee.getGender().toString(), theEmployee.getDateOfBirth(), theEmployee.getEmployeeId());
    }

    @Override
    public void deleteById(int theId) {
        jdbcTemplate.update(
                "DELETE FROM employee WHERE employee_id = ?",
                theId);
    }
}
