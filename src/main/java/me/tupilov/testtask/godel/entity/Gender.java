package me.tupilov.testtask.godel.entity;

public enum Gender {
    MALE,
    FEMALE
}
