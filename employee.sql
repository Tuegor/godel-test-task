DROP TABLE IF EXISTS employee;

CREATE TYPE gender_type AS ENUM ('MALE', 'FEMALE');

CREATE TABLE employee
( 
  employee_id serial,
  first_name varchar(50),
  last_name varchar(50),
  department_id integer,
  job_title varchar(50),
  gender gender_type,
  date_of_birth date, 
  CONSTRAINT employee_id_pk PRIMARY KEY (employee_id)
);
--
-- Data for table employee
--

INSERT INTO employee VALUES 
	(default,'Leslie','Andrews',5,'junior developer','FEMALE','1993-10-23'),
	(default,'Emma','Baumgarten',5,'project manager','FEMALE','1989-02-04'),
	(default,'Avani','Gupta',1,'department head','MALE','1984-05-16'),
	(default,'Yuri','Petrov',2,'accountant','MALE','1981-12-24'),
	(default,'Juan','Vega',4,'janitor','MALE','1991-06-22');

